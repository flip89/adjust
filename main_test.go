package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

// Test that md5 is correctly calculated from response body
func TestCorrectMD5Printed(t *testing.T) {
	testData := `Test String`
	testMD5Sum := md5.Sum([]byte(testData))
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(testData))
	}))
	defer server.Close()

	md5ch := make(chan URLMD5, 1)
	urlCh := make(chan string)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	StartWorkerPool(urlCh, md5ch, 1, wg)
	urlCh <- server.URL
	close(urlCh)
	wg.Wait()
	urlMd5 := <-md5ch
	if urlMd5.MD5Sum != testMD5Sum {
		t.Errorf("expected m5d: %x, got: %x", testMD5Sum, urlMd5.MD5Sum)
	}
}

// Test that url and md5 are printed correctly
func TestCorrectPrintedFormat(t *testing.T) {
	md5ch := make(chan URLMD5)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	buf := bytes.NewBuffer([]byte{})
	go Printer(md5ch, wg, buf)

	url := "http://adjust.com"
	md5Sum := md5.Sum([]byte("test"))
	md5ch <- URLMD5{URL: url, MD5Sum: md5Sum}
	close(md5ch)
	wg.Wait()
	result, err := buf.ReadString('\n')
	if err != nil {
		t.Error("couldn't read result")
	}
	expected := fmt.Sprintf("%s %x\n", url, md5Sum)
	if expected != result {
		t.Errorf("expected output: %s, got: %s", expected, result)
	}
}
