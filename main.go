package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	// RequestTimeout is a timeout used for `get` requests to urls
	RequestTimeout = 10
)

// URLMD5 represents struct that is used for printing results
type URLMD5 struct {
	MD5Sum [md5.Size]byte
	URL    string
}

// DownloadURL issues `get` request to specified url and returns body of a page
func DownloadURL(url string) ([]byte, error) {
	client := http.Client{ // we might pass client as a parametr to function, to reuse one client in one goroutine
		Timeout: time.Duration(RequestTimeout * time.Second),
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// StartWorkerPool spawns goroutines that download content of urls passed through channel
func StartWorkerPool(urlCh <-chan string, md5ch chan<- URLMD5, nWorkers int, wg *sync.WaitGroup) {
	for i := 0; i < nWorkers; i++ {
		go func() {
			defer wg.Done()
			for url := range urlCh {
				body, err := DownloadURL(url)
				if err != nil {
					log.Print(err)
					continue
				}
				md5ch <- URLMD5{
					MD5Sum: md5.Sum(body),
					URL:    url,
				}
			}

		}()
	}
}

// Printer prints content of `URLMD5` structs received from channel
func Printer(md5ch <-chan URLMD5, wg *sync.WaitGroup, w io.Writer) {
	for urlMd5 := range md5ch {
		fmt.Fprintf(w, "%s %x\n", urlMd5.URL, urlMd5.MD5Sum)
	}
	wg.Done()
}

func main() {
	nWorkers := flag.Int("parallel", 10, "number of parallel requests")
	flag.Parse()
	domains := flag.Args()

	md5ch := make(chan URLMD5)
	urlCh := make(chan string)
	wg := new(sync.WaitGroup)
	wg.Add(*nWorkers)
	StartWorkerPool(urlCh, md5ch, *nWorkers, wg)

	printerWg := new(sync.WaitGroup)
	printerWg.Add(1)
	go Printer(md5ch, printerWg, os.Stdout)

	for _, d := range domains {
		if !strings.HasPrefix(d, "http") {
			d = "http://" + d
		}
		urlCh <- d
	}
	close(urlCh)
	// wait for goroutines to download the pages
	wg.Wait()
	close(md5ch)
	// wait until all information is printed
	printerWg.Wait()
}
